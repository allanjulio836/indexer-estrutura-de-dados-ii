# Indexer App

Bem-vindo ao Indexer! Este é um programa simples em JavaScript que cria um índice de palavras a partir de um arquivo de texto e fornece algumas funcionalidades úteis. Antes de começar, certifique-se de ter o [Node.js](https://nodejs.org/) instalado em seu sistema.

## Instalação

1. Clone este repositório para o seu ambiente local.

```bash
git clone https://gitlab.com/allanjulio836/indexer-estrutura-de-dados-ii.git
```

2. Navegue até o diretório do projeto.

```bash
cd indexer-estrutura-de-dados-ii
```

3. Instale as dependências.

```bash
npm install
```

## Uso

### Comandos Disponíveis

1. **--freq**: Exibe as top N palavras e suas frequências no arquivo.

```bash
node main.js indexer --freq N arquivo.txt
```

2. **--freq-word**: Exibe a frequência de uma palavra específica no arquivo.

```bash
node main.js indexer --freq-word palavra arquivo.txt
```

3. **--search**: Exibe uma listagem dos ARQUIVOS mais relevantes encontrados pela busca por TERMO. A listagem é apresentada em ordem descrescente de relevância. TERMO pode conter mais de uma palavra. Neste caso, deve ser indicado entre aspas.

```bash
node main.js indexer --search termo arquivo1.txt arquivo2.txt
```

### Observações Importantes

- Certifique-se de substituir `N`, `palavra` e `termo` pelos valores desejados.
- O programa ignora palavras com menos de 2 caracteres e caracteres que não são letras.

## Exemplos

### Exemplo 1: Exibindo as Top 10 Palavras e Suas Frequências

```bash
node main.js indexer --freq 10 exemplo.txt
```

### Exemplo 2: Exibindo a Frequência da Palavra "coding"

```bash
node main.js indexer --freq-word coding exemplo.txt
```

### Exemplo 3: Procurar por arquivos mais relevantes ao termo "frase":

```bash
node main.js indexer --search frase arquivo1.txt arquivo2.txt
```
