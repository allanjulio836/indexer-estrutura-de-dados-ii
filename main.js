const fs = require("fs");
const stream = require("stream");

// Obtém os índices dos argumentos passados via linha de comando
const ARGV_INDEX = process.argv.indexOf("indexer");
const COMMAND = process.argv[ARGV_INDEX + 1];
const N = process.argv[ARGV_INDEX + 2];
const FILE = process.argv[ARGV_INDEX + 3];

// Variáveis globais para armazenar o tempo de início e término do programa
let startTime, endTime;

if (!FILE && COMMAND !== "--help") {
  console.error(
    "Erro: Nenhum arquivo fornecido. Por favor, forneça o caminho do arquivo."
  );
  process.exit(1);
}

if (COMMAND === "--help" || !COMMAND) {
  // Exibe informações de ajuda se o comando for --help ou se nenhum comando for fornecido
  console.log(`
  Uso: node main.js indexer [comando] [N|palavra] [arquivo]

  Comandos disponíveis:
    --freq          Exibe as top N palavras e suas frequências no arquivo.
    --freq-word     Exibe a frequência de uma palavra específica no arquivo.
    --search        Exibe uma listagem dos ARQUIVOS mais relevantes encontrados pela busca por TERMO. A listagem é apresentada em ordem descrescente de relevância. TERMO pode conter mais de uma palavra. Neste caso, deve ser indicado entre àspas.
    --help          Exibe esta mensagem de ajuda.

  Exemplos:
    - Exibindo as top 10 palavras e suas frequências:
      node main.js indexer --freq 10 exemplo.txt

    - Exibindo a frequência da palavra "coding":
      node main.js indexer --freq-word coding exemplo.txt

    - Procurar por arquvivos mais relevantes ao termo: 
      node main.js indexer --search "is a" arquivo1.txt arquivo2.txt

  Observações Importantes:
    - Certifique-se de substituir N, palavra e termo pelos valores desejados.
    - O programa ignora palavras com menos de 2 caracteres e caracteres que não são letras.
  `);
  process.exit(0);
}

// Função para iniciar o cronômetro
function startTimer() {
  startTime = process.hrtime();
}

// Função para parar o cronômetro e exibir o tempo decorrido
function stopTimer() {
  endTime = process.hrtime(startTime);
  const elapsedTime = (endTime[0] + endTime[1] / 1e9).toFixed(3);
  console.log(`Tempo de processamento: ${elapsedTime} segundos`);
}

function calculateTF(wordIndex, length) {
  const tf = {};
  for (let word in wordIndex) {
    tf[word] = wordIndex[word] / length;
  }
  return tf;
}

function calculateIDF(wordIndexGroup) {
  const idf = {};

  for (let wordIndex of wordIndexGroup) {
    for (let word in wordIndex) {
      idf[word] = (idf[word] || 0) + 1;
    }
  }

  return idf;
}

function calculateTFIDF(tf, idf) {
  const tfidf = {};
  for (let word in tf) {
    tfidf[word] = tf[word] * (idf[word] || 0);
  }
  return tfidf;
}

// Função assíncrona que realiza a busca usando o cálculo TF-IDF
async function performSearch(term, files) {
  // Obtém os índices de palavras para cada arquivo
  const wordIndexGroup = await Promise.all(files.map(createWordIndex));

  // Calcula o TF-IDF para cada arquivo
  const tfidfResults = wordIndexGroup.map((wordIndex) => {
    const tf = calculateTF(wordIndex, Object.keys(wordIndex).length);
    const idf = calculateIDF(wordIndexGroup);
    const tfidf = calculateTFIDF(tf, idf);

    return tfidf;
  });

  // Calcula a relevância de cada arquivo em relação ao termo de busca
  const fileRelevance = tfidfResults.map((tfidf) => {
    let relevance = 0;
    for (let word of term) {
      relevance += tfidf[word] || 0;
    }
    return relevance;
  });

  // Cria um array de objetos contendo o nome do arquivo e sua relevância
  const resultArray = files.map((file, index) => ({
    file: file,
    relevance: fileRelevance[index],
  }));

  // Ordena os resultados por relevância em ordem decrescente
  const sortedResults = resultArray.sort((a, b) => b.relevance - a.relevance);

  // Imprime os resultados
  sortedResults.forEach((result) =>
    console.log(`\n${result.file}: Relevância - ${result.relevance.toFixed(3)}`)
  );
}

// Função que cria um índice de palavras a partir de um arquivo
function createWordIndex(file) {
  return new Promise((resolve, reject) => {
    const wordIndex = {};

    // Cria um stream de transformação para processar cada linha
    const lineTransform = new stream.Transform({
      transform(chunk, encoding, callback) {
        // Converte o pedaço (chunk) em uma string
        const line = chunk.toString();

        // Encontra palavras na linha, converte para minúsculas e filtra
        const words = line.toLowerCase().match(/\b(\w{2,})\b/g);

        if (words) {
          // Itera sobre as palavras encontradas na linha
          words.forEach((word) => {
            // Remove caracteres que não são letras
            const filteredWord = word.replace(/[^a-z]/g, "");

            // Ignora palavras com menos de 2 caracteres
            if (filteredWord.length >= 2) {
              // Atualiza o índice de palavras
              wordIndex[filteredWord] = (wordIndex[filteredWord] || 0) + 1;
            }
          });
        }

        // Chama a função de callback para indicar o processamento do chunk
        callback();
      },
    });

    // Cria um stream legível a partir do arquivo
    const fileStream = fs.createReadStream(file);

    // Encadeia o stream do arquivo com o stream de transformação de linha
    fileStream.pipe(lineTransform);

    // Evento disparado quando o stream do arquivo é concluído
    fileStream.on("close", function () {
      resolve(wordIndex);
      stopTimer();
    });

    // Evento disparado em caso de erro na leitura do arquivo
    fileStream.on("error", function (error) {
      reject(error);
    });
  });
}

// Função assíncrona que imprime as top N palavras e suas frequências
async function printWordFrequencies() {
  if (N < 1) {
    console.log("O número informado deve ser maior que 0");
    process.exit(0);
  }
  // Obtém o índice de palavras a partir do arquivo
  const wordIndex = await createWordIndex(FILE);

  // Ordena as palavras por frequência e imprime as top N
  const sortedWords = Object.entries(wordIndex).sort((a, b) => b[1] - a[1]);
  sortedWords
    .slice(0, N)
    .forEach(([word, freq]) => console.log(`${word}: ${freq}`));
}

// Função assíncrona que imprime a frequência de uma palavra específica
async function printWordFrequency() {
  // Obtém o índice de palavras a partir do arquivo
  const wordIndex = await createWordIndex(FILE);

  // Normaliza a palavra para minúsculas, pois o índice é case-insensitive
  const normalizedN = N.toLowerCase();

  // Imprime a frequência da palavra especificada
  console.log(`${normalizedN}: ${wordIndex[normalizedN] || 0}`);
}

// Função assíncrona que executa a busca usando o cálculo TF-IDF
async function searchFiles() {
  // Converte o termo de busca para minúsculo
  const searchTerm = N.toLowerCase();

  // Obtém a lista de arquivos a serem pesquisados
  const filesToSearch = process.argv.slice(ARGV_INDEX + 3);

  // Realiza a busca e imprime os resultados
  await performSearch([searchTerm], filesToSearch);
}

// Inicia o cronômetro antes de executar os comandos
startTimer();

// Executa a lógica correspondente ao comando passado via linha de comando
switch (COMMAND) {
  case "--freq":
    printWordFrequencies();
    break;
  case "--freq-word":
    printWordFrequency();
    break;
  case "--search":
    searchFiles();
    break;
  default:
    console.log("Comando não encontrado.");
}
